# contains-reference

Checks to see if any iterable object or array reference in two objects or arrays match.

Useful to ensure that a "copied" object does not inadvertently affect distant areas of code when modified.

### Usage

```js
const assert = require('assert')
const containsReference = require('contains-reference')

// Object structure and primitives are the same,
// but the object references are different
const object1 = { foo: 'bar', inner: { a: 'b' } }
const object2 = { foo: 'bar', inner: { a: 'b' } }

assert(!containsReference(object1, object2)) // false

// This array contains a reference to the first object now
const someArray = [object1]

assert(containsReference(someArray, object1)) // true
assert(!containsReference(someArray, object2)) // false

// the second object now contains a reference to the first,
// and by extension the array also has a reference to the second object
object1.inner = object2.inner

assert(containsReference(object1, object2)) // true
assert(containsReference(someArray, object1)) // true
assert(containsReference(someArray, object2)) // true
```
