const isIterable = o => o != null && o instanceof Object
const getReferences = o => {
  let referenceArray = []
  if (isIterable(o)) {
    referenceArray.push(o)
    for (const ref in o) {
      referenceArray = referenceArray.concat(getReferences(o[ref]))
    }
  }
  return referenceArray
}
const containsReference = (o1, o2) => {
  const refs1 = getReferences(o1)
  const refs2 = getReferences(o2)
  return refs1.some(ref1 => refs2.some(ref2 => ref1 === ref2))
}
module.exports = containsReference
