/* eslint-env mocha */
const assert = require('assert')
const containsReference = require('./index.js')

describe('Totally Different Tests', function () {
  it('top level, different objects', function () {
    const o1 = {}
    const o2 = {}
    assert(!containsReference(o1, o2))
  })
  it('top level, same object', function () {
    const o1 = {}
    const o2 = o1
    assert(containsReference(o1, o2))
  })
  it('top level, different arrays', function () {
    const a1 = []
    const a2 = []
    assert(!containsReference(a1, a2))
  })
  it('top level, same arrays', function () {
    const a1 = []
    const a2 = a1
    assert(containsReference(a1, a2))
  })
  it('object-in-object, different', function () {
    const o1 = { inner: {} }
    const o2 = { inner: {} }
    assert(!containsReference(o1, o2))
  })
  it('object-in-object, shared reference', function () {
    const o1 = { inner: {} }
    const o2 = { inner: o1.inner }
    assert(containsReference(o1, o2))
  })
  it('object-in-array, different', function () {
    const a1 = [{}]
    const a2 = [{}]
    assert(!containsReference(a1, a2))
  })
  it('object-in-array, shared reference', function () {
    const a1 = [{}]
    const a2 = [a1[0]]
    assert(containsReference(a1, a2))
  })

  it('array-in-object, different', function () {
    const o1 = { inner: [] }
    const o2 = { inner: [] }
    assert(!containsReference(o1, o2))
  })
  it('array-in-object, shared reference', function () {
    const o1 = { inner: [] }
    const o2 = { inner: o1.inner }
    assert(containsReference(o1, o2))
  })
  it('array-in-array, different', function () {
    const a1 = [[]]
    const a2 = [[]]
    assert(!containsReference(a1, a2))
  })
  it('array-in-array, shared reference', function () {
    const a1 = [[]]
    const a2 = [a1[0]]
    assert(containsReference(a1, a2))
  })
  it('random, different', function () {
    const o1 = {
      foo: 'bar',
      arr: [
        1,
        2,
        3,
        {
          innerProp: 'value'
        }
      ]
    }
    const o2 = [
      {
        objIsDifferent: 'yes, yes it is',
        innerArr: [
          'a',
          'b',
          'c'
        ]
      }
    ]
    assert(!containsReference(o1, o2))
  })
  it('random, shared reference 1', function () {
    const o1 = {
      foo: 'bar',
      arr: [
        1,
        2,
        3,
        {
          innerProp: 'value'
        }
      ]
    }
    const o2 = [
      {
        objIsDifferent: 'yes, yes it is',
        innerArr: [
          'a',
          'b',
          o1.arr
        ]
      }
    ]
    assert(containsReference(o1, o2))
  })
  it('random, shared reference 2', function () {
    const o1 = {
      foo: 'bar',
      arr: [
        1,
        2,
        3,
        {
          innerProp: 'value'
        }
      ]
    }
    const o2 = [
      {
        objIsDifferent: o1.arr[3],
        innerArr: [
          'a',
          'b',
          'c'
        ]
      }
    ]
    assert(containsReference(o1, o2))
  })
  it('README sample', function () {
    // Object structure and primitives are the same,
    // but the object references are different
    const object1 = { foo: 'bar', inner: { a: 'b' } }
    const object2 = { foo: 'bar', inner: { a: 'b' } }

    assert(!containsReference(object1, object2)) // false

    // This array contains a reference to the first object now
    const someArray = [object1]

    assert(containsReference(someArray, object1)) // true
    assert(!containsReference(someArray, object2)) // false

    // the second object now contains a reference to the first,
    // and by extension the array also has a reference to the second object
    object1.inner = object2.inner

    assert(containsReference(object1, object2)) // true
    assert(containsReference(someArray, object1)) // true
    assert(containsReference(someArray, object2)) // true
  })
})
